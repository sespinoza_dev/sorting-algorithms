const { expect } = require('chai')
const bubbleSort = require('../src/bubble-sort')
const quicksort = require('../src/quick-sort')

describe('Bubble sort', () => {
  it('should sort an unsorted array', () => {
    const array = [5,2,7,4]
    const expected = [2,4,5,7]
    const actual = bubbleSort(array)
    expect(actual).to.be.deep.eq(expected)
  })
})

describe('Quicksort', () => {
  it('should sort an unsorted array', () => {
    const array = [5,2,7,4]
    const expected = [2,4,5,7]
    const actual = quicksort(array)
    console.log(actual)
    expect(actual).to.be.deep.eq(expected)
  })
})