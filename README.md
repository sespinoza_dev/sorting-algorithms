# Sorting Algorithms

A review of sorting algorithms implementations. As a way of relearning them 
and get deep into time and space complexity.

## Getting Started

Here is the list of sorting algorithms that I'll cover:

 * Bubble sort.
 * Quicksort.
 * Merge sort.

### Prerequisites

For running these algorithms you will need to have `Nodejs` installed.

To run each file like this:

```
node src/quick-sort.js
```

## Running the tests

To run the test you will need to install the dev dependencies (mocha and chai).

```console
npm install
```

After that to run the test you only have to run

```console
npm run test
```

## Contributing

1. Clone the project (`git clone git@github.com:sespinoza-dev/sorting-algorithms.git && cd searching-algorithms`)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am "Add some feature"`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request/Pull request.

## Authors

* **Samuel Espinoza** - [sespinoza](https://github.com/sespinoza-dev)

## Acknowledgements

- Special thanks to [Daniel Salazar](https://github.com/disalazarg/) for reviewing my code.
