
const swap = (array, firstIndex, secondIndex) => {
  // console.log(`swaping ${array[firstIndex]} with ${array[secondIndex]}`)
  const tmp = array[secondIndex]
  array[secondIndex] = array[firstIndex]
  array[firstIndex] = tmp
}

/**
 * sort a list from lowest to highest
 * @param  {Array}   list list of integers
 * @return {Array}  sorted list
*/
const quicksort = list => {
  if (list.length == 0) return []
  console.log(`\niterating on [${list}]`)

  const pivotPosition = Math.floor(list.length / 2)
  const pivot = list[pivotPosition]
  console.log(`pivot: ${pivot}`)
  swap(list, pivotPosition, list.length -1)
  console.log(list)
  let i = 0
  for (let j = 0; j < list.length - 1; j++) {
    if (list[j] < pivot) {
      swap(list, j, i)
      i++
      console.log(list)
    }
  }
  swap(list, i, list.length - 1)
  return [...quicksort(list.slice(0, i)), list[i], ...quicksort(list.slice(i + 1))];
}

const array = [5,2,7,4]
const actual = quicksort(array)
console.log('actual: ', actual)

module.exports = quicksort