
const swap = (array, firstIndex, secondIndex) => {
  console.log(`swaping ${array[firstIndex]} with ${array[secondIndex]}`)
  let tmp = array[secondIndex]
  array[secondIndex] = array[firstIndex]
  array[firstIndex] = tmp
}

const bubbleSort = array => {
  console.log('input: ', array)

  let sortedArray = array
  for (let i = 0; i < (array.length - 1) -i; i++) {
    console.log('\nnew iteration: ', sortedArray, '\n')
    for (let j = 0; j < array.length -1; j++) {

      if (array[j] > array[j+1]) {
        swap(array, j, j+1) 
      };
      
    }
  }
  console.log('sorted: ', sortedArray)
  return sortedArray
}

module.exports = bubbleSort